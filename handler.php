<?php
// File:        handler.php
// Author:      Joe Moss
// Date:        12-Dec-2018
// Purpose:     This is the controller for the network visuaizer; it coordinates
//              requests from the front-end, interacts with the data model,
//              and presents the correct UI (signup, login, or visualizer.php).


require_once("model.php");



// Maps actions to the functions that will carry them out.
$ACTION_MAP = array(
    "signup"        => "signup", 
    "login"         => "login", 
    "logout"        => "logout",
    "upload_settings" => "uploadSettings",
    "download_settings" => "getSelectedUserSettings"
);

// Figure out current user status.
$loggedInUser = null;
if(array_key_exists("TODOID", $_COOKIE)){
    if($_COOKIE["TODOID"] != ""){
        $user = getUserInfoByAuthToken($_COOKIE["TODOID"]);
        if($user != null){
            $loggedInUser = $user["id"];
        }
    }
}

// Call the correct action.
if(array_key_exists("action", $_POST) && 
        array_key_exists($_POST["action"], $ACTION_MAP)){
    $ACTION_MAP[$_POST["action"]]($_POST);
} elseif($loggedInUser == null){
    redirectToLogin();
}

// Load the todo list.
if(!array_key_exists("action", $_POST) || $_POST["action"] == "login")
    // Load the todo list.
    require_once("visualizer.php");
else 
    print json_encode(["success" => true]);





// FUNCTIONS

/**
 * Signs the user up. Requires that $data has the following keys:
 *  - username
 *  - password
 */
function signup($data){
    logout($data, false);

    // Make sure the username is okay.
    $user = getUserInfo($data["username"]);
    if($user != null){
        redirectToSignup("That username is taken. Try a different one.");
    }
    if($data["password"] == $data["password2"]){
        // Add the user.
        addNewUser($data["username"], 
            password_hash($data["password"], PASSWORD_BCRYPT));

        // Redirect them to the login page.
        redirectToLogin("", "Account created! Now log in.");
    }else{
        redirectToSignup("Your passwords did not match, please try again.");
    }
}

/**
 * Logs the user in. Requires that $data has the following keys:
 *  - username
 *  - password
 */
function login($data){
    global $loggedInUser;
    logout($data, false);

    // Authenticate the user.
    $user = getUserInfo($data["username"]);

    // TODO #3: Complete this condition to authenticate the user.
    if(!password_verify($data["password"], $user["password"])){
        redirectToLogin("Invalid username or password.");
    }

    // Generate an authentication token.
    $loggedInUser = $user["id"];
    $authToken = bin2hex(random_bytes(25));
    setUserAuthToken($loggedInUser, $authToken);

    // TODO #4: Make it so that the cookie lasts two weeks if the users
    // checked "Remember me".
    if (isset($_POST["remember-me"])) {
        setcookie("TODOID", $authToken, time()+86400*14);
    }else{
        setcookie("TODOID", $authToken);
    }
    // redirectToVisualizer();
}

/**
 * Logs the user out.
 */
function logout($data, $redirect=true){
    global $loggedInUser;

    if($loggedInUser != null){
        setUserAuthToken($loggedInUser, "");
        setcookie("TODOID", "", time() - 3600);
    }

    if($redirect)
        redirectToLogin();
}

/**
 * If no user is logged in, redirects to the login page.
 */
function bootUserIfNotLoggedIn(){
    global $loggedInUser;
    if($loggedInUser == null){
        redirectToLogin();
    }
}

function uploadSettings($data){

    global $loggedInUser;
    bootUserIfNotLoggedIn();
    addNewUserSettings($loggedInUser, $data["settingsText"], $data["settingsTitle"]);

}

function getSelectedUserSettings($data) {
    global $loggedInUser;
    bootUserIfNotLoggedIn();
    getUserSettings($loggedInUser, $data['settings_title']);
    
}
// function downloadSettings($data){

//     global $loggedInUser;
//     bootUserIfNotLoggedIn();

//     $userSettings = GetuserSettings($loggedInUser);

// }


function redirectToLogin($error="", $message="") {
    echo '
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="refresh" content="0; url=login.html?error='. 
        rawurlencode($error) .'&message='. rawurlencode($message) .'" />
    </head>
    </html>
    <html>
    ';
    exit();
}

function redirectToVisualizer(){
        echo '
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="refresh" content="0; url=visualizer.php" />
    </head>
    </html>
    <html>
    ';

}

/**
 * Redirects to the signup page, adding in provided errors or messages.
 */
function redirectToSignup($error="", $message="") {
    echo '
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="refresh" content="0; url=signup.html?error='. 
        rawurlencode($error) .'&message='. rawurlencode($message) .'" />
    </head>
    </html>
    <html>
    ';
    exit();
}


?>
