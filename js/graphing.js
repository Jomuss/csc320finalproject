// File:        graphing.js
// Author:      Joe Moss
// Date:        12-Dec-2018
// Purpose:     This is the file that handles all of the graphing and table generation


//Initialize variables
var svg;
var nodelookup = {};
var nodeCollector = {};
var linkCollector = {};
var linkchecker = {};
var nodeKeys = {};
var edgeKeys = {};
var settings = {
		sourceColumn : "N/A",
		targetColumn : "N/A",
		colorColumn : "N/A",
		labelColumn : "N/A",
		showNodeLabels : true,
		edgeColor : "rgb(0,0,0)"
}

var numEdges = 0;
var numNodes = 0;
var curNodeColor;
var nodeColor;
var edgeColor;
var colorColumn = null;
var labelColumn = null;
var nodeFileName = null;
var edgeFileName = null;



var uploadNodesToGraph = function(event){
	nodelookup = {};
	nodeCollector = {};
	numNodes = 0;

	event.preventDefault();
	//Modified from code found at https://www.html5rocks.com/en/tutorials/file/dndfiles/
	var file = document.getElementById('nodeFileInput').files[0];

	nodeFileName = file["name"];

	var fileReader = new FileReader();
	fileReader.onload = (function(file){
		return function(e){
			var fileText = e.target.result;
			
			var nodeHolder = parseFile(nodeFileName, fileText);
		
			nodeKeys = Object.keys(nodeHolder[0]);
			
			if(nodeTable != null){
				d3.selectAll('.nodeTableElement').remove();
			}

			nodeTable = generateTable(nodeHolder, nodeKeys, d3.select('#nodeTable'), 'nodeTableElement', d3.selectAll('.nodeSelector'));
			
			var count = 0; 
			//Modified from code found at http://bl.ocks.org/ccmcc/5182685
			nodeHolder.forEach(function(row){
				nodelookup[row[nodeKeys[0]]] = count;
			

				nodeCollector[row[nodeKeys[0]]] = {[nodeKeys[0]]: row[nodeKeys[0]]};
				if(nodeKeys.length > 1){
					for(var i=0; i<nodeKeys.length; i++){
						Object.assign(nodeCollector[row.node], {[nodeKeys[i]]: row[nodeKeys[i]]});
					}
				}
				

				count++;
				numNodes++;
			});
	
		};
	})();
	fileReader.readAsText(file);
	
};

var uploadEdgesToGraph = function(event){
	event.preventDefault();
	var file = document.getElementById('edgeFileInput').files[0];
	numEdges = 0;
	
	
	edgeFileName = file["name"];
	//Modified from code found at https://www.html5rocks.com/en/tutorials/file/dndfiles/
	var fileReader = new FileReader();
	fileReader.onload = (function(file){
		return function(e){
			var fileText = e.target.result;
			linkChecker = parseFile(edgeFileName, fileText);
		
			edgeKeys = Object.keys(linkChecker[0]);

			
			if(edgeTable != null){
				d3.selectAll('.edgeTableElement').remove();
			}
			edgeTable = generateTable(linkChecker, edgeKeys, d3.select('#edgeTable'), 'edgeTableElement', d3.selectAll(".edgeSelector"));

			
			
		};
	})();
	fileReader.readAsText(file);		
}
	
/**
Params:
data: the column and row data for the table
columns: the names of the table columns
table: the html table that will hold the data
tableElements: the name of the classs you wish to give to the table elements. Used for clearing the table when resubmitting new data

**/
//Modfied from code found at http://bl.ocks.org/jfreels/6734025
var generateTable = function(data, columns, table, tableElements, selectors) {



	
	var thead = table.append('thead')
					 .attr('class', tableElements);

	var	tbody = table.append('tbody')
					 .attr('class', tableElements);


	selectors.selectAll('option')
				.data(columns, function(d){return d;}).enter()
				.append('option')
				.text(function (column) { return column; });
				

	// append the header row
	thead.append('tr')
	  .selectAll('th')
	  .data(columns).enter()
	  .append('th')
	    .text(function (column) { return column; });

	// create a row for each object in the data
	var rows = tbody.selectAll('tr')
	  .data(data)
	  .enter()
	  .append('tr');

	// create a cell in each row for each column
	var cells = rows.selectAll('td')
	  .data(function (row) {
	    return columns.map(function (column) {
	      return {column: column, value: row[column]};
	    });
	  })
	  .enter()
	  .append('td')
	    .text(function (d) { return d.value; });

  return table;
}


//Takes care in Initializing the graph
//TODO: Maybe refactor, seems long
function refreshGraph(){

}

function parseFile(fileName, fileText){
	fileType = fileName.slice(-3);
	if(fileType === "csv"){
		return d3.csv.parse(fileText); 
	}else if (fileType === 'tsv'){
		return d3.tsv.parse(fileText);
	}
}	
function initializeGraph(){
	var nodesByName = {};
	linkCollector = {};
	var colors = {};
	var toggle = 0;

	//Get Source and Target columns from selctors, if N/A use defaults (1st column for source, 2nd for target)
	var sourceColumn = ($('#sourceSelector :selected').text() == "N/A") ? [edgeKeys[0]] : $('#sourceSelector :selected').text();
	settings.sourceColumn = sourceColumn;
	var targetColumn = ($('#destinationSelector :selected').text() == "N/A") ? [edgeKeys[1]] : $('#destinationSelector :selected').text();
	settings.targetColumn = targetColumn;


	//Modified from code found at http://bl.ocks.org/ccmcc/5182685
	//Create links using node data and columns
	var count = 0;
	numEdges = 0;
	linkChecker.forEach(function(link) {
		linkCollector[count] = {source: nodelookup[link[sourceColumn]], target: nodelookup[link[targetColumn]]}
		numEdges = numEdges+1;
		count++;
	});

	//Get label and color coulmns, use defaults of N/A
	var labelColumn = ($('#labelSelector :selected').text() == "N/A") ? [nodeKeys[0]] : $('#labelSelector :selected').text();
	settings.labelColumn = labelColumn;
	var colorColumn = ($('#colorSelector :selected').text() == "N/A") ? "N/A" : $('#colorSelector :selected').text();
	settings.colorColumn = colorColumn;



	var width = 500, height = 500;
	if(svg!=null){
		d3.select("svg").remove();
	}
	// Some code taken from: https://bl.ocks.org/mbostock/2949937
	//Create svg
	svg = d3.select("#graph-container").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	    .attr("id", "graph-svg")
	    .attr("style", "outline: thick solid black;");

	var force = d3.layout.force()
		.size([width, height]);
	
	

	var nodes = d3.values(nodeCollector);
	var links = d3.values(linkCollector);



	$("#numEdges").text("Number of Edges: "+numEdges);
	$("#numNodes").text("Number of Nodes: "+numNodes);

	// Create the link lines.
	
	var link = svg.selectAll(".link")
	  .data(links)
	.enter().append("line")
	  .attr("class", "link")
	  .attr("stroke", settings["edgeColor"]);

	// Create the node circles.
	var node = svg.selectAll(".node")
	  .data(nodes)
	.enter().append("circle")
	  .attr("class", "node")
	  .attr("x", function (d) { return d.x; })
      .attr("y", function (d) { return d.y; })
	  .attr("r", 10)
	  .style("fill", function(d) { return (colorColumn == null) ? "black" : d[colorColumn]})
	  .call(force.drag)
	  .on('dblclick', connectedNodes);

	//https://www.dashingd3js.com/svg-text-element
	var text = svg.selectAll("text")
		.data(nodes)
		.enter()
		.append("text");

	var textLabels = text
    	.attr("x", function(d) {  return -20; })
    	.attr("y", function(d) { return  +10; })
    	.text(function(d) { return d[labelColumn]; })
    	.attr("font-family", "sans-serif")
        .attr("font-size", "14px")
        .style("fill", "black")
        .attr("class", "textLabel");
	// Start the force layout.
	force
	  .nodes(nodes)
	  .links(links)
	  .on("tick", tick)
	  .linkDistance(100)
	  .gravity(.01)
	  .charge(-50)
	  .start();

	function tick() {
	link.attr("x1", function(d) { return d.source.x; })
	    .attr("y1", function(d) { return d.source.y; })
	    .attr("x2", function(d) { return d.target.x; })
	    .attr("y2", function(d) { return d.target.y; });

	node.attr("cx", function(d) { return d.x; })
	    .attr("cy", function(d) { return d.y; });

	textLabels.attr("x", function(d) { return d.x+6; })
			.attr("y", function(d) { return d.y-10; })
	}

	//Code taken/modified from http://www.coppelia.io/2014/06/finding-neighbours-in-a-d3-force-directed-layout-2/
	var linkedByIndex = {};
	for (i = 0; i < nodes.length; i++) {
	    linkedByIndex[i + "," + i] = 1;
	};
	links.forEach(function (d) {
	    linkedByIndex[d.source.index + "," + d.target.index] = 1;
	});
	function neighboring(a, b) {
	    return linkedByIndex[a.index + "," + b.index];
	}

	function connectedNodes() {

	    if (toggle == 0) {

	        d = d3.select(this).node().__data__;

	        node.style("opacity", function (o) {
	            return neighboring(d, o) | neighboring(o, d) ? 1 : 0.15;
	        });
	        node.style("fill", function (o) {
	            return neighboring(d, o) | neighboring(o, d) ? "orange" : o[colorColumn];
	        });
	        d3.select(this).style("fill", "yellow");

	        nodeKeys.forEach(function (column){
	        	
	        	d3.select("#selectedNodeData")
	        	  .append("p")
	        	  	.text(column+": "+d[column])
	        	  	.attr("id", "currentNodeData");
	        });

	        toggle = 1;
	    } else {
	        node.style("opacity", 1);
	        node.style("fill", function(d) { return d[colorColumn]; });
	        link.style("opacity", 1)
	        toggle = 0;
	        d3.selectAll("#currentNodeData").remove();
	    }

	}

	$("#graph-settings").show();
}

var updateSettings = function(){
	if($("#names-toggle").is(":checked")){
		$(".textLabel").show();
		settings.showNodeLabels = true;
	}else{
		$(".textLabel").hide();
		settings.showNodeLabels = false
	}
	
	if(settings["edgeColor"] != $('#edge-color').val()){
		settings["edgeColor"] = rgbaHex($('#edge-color').val());
		$(".link").css('stroke', settings["edgeColor"]);
	}
	console.log(settings);
}

var updateSettingsFromSelected = function(settingsData){
	var optionExists = false;

	//Iterate through options of Color Selector to check if settings option exists
	for(var i=0; i < $("#colorSelector").children().length; i++){
		($("#colorSelector").children().eq(i).prop("selected", false));

		if($("#colorSelector").children().eq(i).val() == settingsData["colorColumn"]){
			optionExists = true;
			($("#colorSelector").children().eq(i).prop("selected", true));
			settings.colorColumn = settingsData["colorColumn"];


		}
	}

	//Do the same for the label selector
	optionsExists = false;
	for(var i=0; i < $("#labelSelector").children().length; i++){
		($("#labelSelector").children().eq(i).prop("selected", false))

		if($("#labelSelector").children().eq(i).val() == settingsData["labelColumn"]){
			optionExists = true;
			($("#labelSelector").children().eq(i).prop("selected", true));
			settings.labelColumn = settingsData["labelColumn"];

		}
	}


	//Same for Source Selector
	optionsExists = false;
	for(var i=0; i < $("#sourceSelector").children().length; i++){
		($("#sourceSelector").children().eq(i).prop("selected", false));

		if($("#sourceSelector").children().eq(i).val() == settingsData["sourceColumn"]){
			optionExists = true;
			($("#sourceSelector").children().eq(i).prop("selected", true));
			settings.sourceColumn = settingsData["sourceColumn"];
		}
	}


	//Same for Target selector
	optionsExists = false;
	for(var i=0; i < $("#destinationSelector").children().length; i++){
		($("#destinationSelector").children().eq(i).prop("selected", false));

		if($("#destinationSelector").children().eq(i).val() == settingsData["targetColumn"]){
			optionExists = true;
			($("#destinationSelector").children().eq(i).prop("selected", true));
			settings.targetColumn = settingsData["targetColumn"];
		}
	}


	settings.showNodeLabels = settingsData["showNodeLabels"];
	if(settingsData["showNodeLabels"]){
		$("#names-toggle").prop(':checked', true);
	}else{
		$("#names-toggle").prop(':checked', false);
	}

	// settings["edgeColor"] = settingsData["edgeColor"];
	$('#edge-color').prop('value', rgbaHex(settingsData["edgeColor"]));
	initializeGraph();
	updateSettings();
	}



// This function converts hex values to rgb, taken from https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb?page=2&tab=votes
function rgbaHex(c,a,i){return(Array.isArray(c)||(typeof c==='string'&&/,/.test(c)))?((c=(Array.isArray(c)?c:c.replace(/[\sa-z\(\);]+/gi,'').split(',')).map(s=>parseInt(s).toString(16).replace(/^([a-z\d])$/i,'0$1'))),'#'+c[0]+c[1]+c[2]):(c=c.replace(/#/,''),c=c.length%6?c.replace(/(.)(.)(.)/,'$1$1$2$2$3$3'):c,a=parseFloat(a)||null,`rgb${a?'a':''}(${[(i=parseInt(c,16))>>16&255,i>>8&255,i&255,a].join().replace(/,$/,'')})`);}
	
$(document).ready(function() {
	
	$(document).on("click", "#refresh-button", updateSettings);
	$(document).on("click", "#graphInitButton", initializeGraph);
	$(document).on("click", '#download-graph-button', function(){ $('#download-modal').modal('show'); });
	$(document).on("click", '#upload-settings-modal-button', function(){ $('#upload-settings-modal').modal('show'); });
	$(document).on("click", "#download-png-button", function(){
		saveSvgAsPng(document.getElementById("graph-svg"), "graph.png");
	})

	document.getElementById('nodeFileInput').addEventListener('change', uploadNodesToGraph);
	document.getElementById('edgeFileInput').addEventListener('change', uploadEdgesToGraph);


});