<?php
// File:        model.php
// Author:      Joe Moss

// Date:        12-dec-2018
// Purpose:     Handles the data for the Network Visualizer app.


// Information about the database. Usually you'd keep this in a file in a
// non-web-accessible directory on the server. That way, if PHP goes down
// for some reason and the actual PHP code is exposed, the database
// details (which often include credentials) are not exposed.


$dbName = "/home/jmoss/csc302-fa18-data/graphing2.db";
$dsn = "sqlite:$dbName";
$dbh = null;


function error($errorMessage){
    die(json_encode([
        "success" => false,
        "error" => $errorMessage
    ]));
}


/**
 * Attempts to make a connection the database if one doesn't already exist, 
 * failing otherwise. Uses the $dsn global and sets the $dbh global.
 */
function connectToDB(){
    global $dsn;
    global $dbh;

    try{
        if($dbh == null){
            $dbh = new PDO($dsn);
            setupDB();
        }
    } catch(PDOException $e) {
        die("Couldn't establish a connection to the database: ". 
            $e->getMessage());
    }
}

/**
 * Sets up the database, creating the users and todo_items tables if they're 
 * not already there.
 */
function setupDB(){
    global $dbh;
    try {
        // Set up the users table.
        $dbh->exec("CREATE table if not exists users(".
            "id integer primary key autoincrement,".
            "username text,".
            "password text,".
            "auth_token text)");

        // Check for errors.
        $error = $dbh->errorInfo();
        if($error[0] !== '00000' && $error[0] !== '01000') {
            die("There was an error setting up the users table: ". $error[2]);
        }

        // Setup the settings.
        $dbh->exec("CREATE table if not exists user_settings(".
            "id integer primary key autoincrement,".
            "user_id integer,".
            "settings_title text,".
            "settings_text text)");

        // Check for errors.
        $error = $dbh->errorInfo();
        if($error[0] !== '00000' && $error[0] !== '01000') {
            die("There was an error setting up the settings table: ". $error[2]);
        }
    } catch(PDOException $e) {
        die("There was an error setting up the database: ". $e->getMessage());
    }
}

/**
 * Saves the data for the given username. 
 * 
 * @param username The username.
 * @param password The password hash for the user.
 */
function addNewUser($username, $password){
    global $dbh;
    connectToDB();

        try {
            $statement = $dbh->prepare("INSERT INTO users(username, password) ".
                "values(:username, :password)");

            $success = $statement->execute(array(
                ":username" => $username, 
                ":password" => $password));
               
            if(!$success){
                die("There was an error saving to the database: ". 
                    $dbh->errorInfo()[2]);
            }
        } catch(PDOException $e){
            die("There was an error saving to the database: ". $e->getMessage());
        }
}

/**
 * Retrieves data for the given username. 
 * 
 * @param username The username to lookup.
 * @return The data associated with the given username, or null if the username
 *         doesn't exist. The returned data is an associative array with these
 *         fields:
 *           - id
 *           - username
 *           - password
 *           - auth_token
 */
function getUserInfo($username){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "SELECT * FROM users WHERE username = :username");
        $success = $statement->execute(array(
            ":username" => $username));
           
        if(!$success){
            die("There was an error reading from the database: ". 
                $dbh->errorInfo()[2]);
        } else {
            return $statement->fetch(PDO::FETCH_ASSOC);
        }
    } catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
}

function getUserSettings($userId, $settingsName){
    global $dbh;
    connectToDB();

    try{
        $statement = $dbh->prepare(
            "SELECT settings_text FROM user_settings WHERE user_id = :user_id AND settings_title = :settings_title");
        $success = $statement->execute(array(
            ":user_id" => $userId,
            ":settings_title" => $settingsName));
        if(!$success){
            die("There was an error reading from the database: ".
                $dbh->errorInfo[2]);
        } else{
            $settingsText = $statement->fetchAll(PDO::FETCH_ASSOC)[0];
            echo $settingsText["settings_text"];
        }
    } catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
}

function getUserSettingsNames($userId){
    global $dbh;
    connectToDB();

    try{
        $statement = $dbh->prepare(
            "SELECT settings_title FROM user_settings WHERE user_id = :user_id");
        $success = $statement->execute(array(
            ":user_id" => $userId));
        if(!$success){
            die("There was an error reading from the database: ".
                $dbh->errorInfo[2]);
        }else{
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
    }catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
    
}

function addNewUserSettings($userId, $settingsText, $settingsTitle){
    global $dbh;
    connectToDB();


    try {
        $statement = $dbh->prepare(
            "INSERT INTO user_settings(user_id, settings_title, settings_text) ".
            "values(:user_id, :settings_title, :settings_text)");
        if(!$statement){
            die("There was an error saving to the database: ". 
                $dbh->errorInfo()[2]);
        }
        $success = $statement->execute(array(
            ":user_id" => $userId,
            ":settings_title" => $settingsTitle,
            ":settings_text" => $settingsText));

        if(!$success){
            die("There was an error saving to the database: ". 
                $dbh->errorInfo()[2]);
        }
    } catch(PDOException $e){
        die("There was an error saving to the database: ". $e->getMessage());
    }
}

/**
 * Retrieves data for the given auth token. 
 * 
 * @param auth_token The auth token to look the user up by.
 * @return The data associated with the given username, or null if the username
 *         doesn't exist. The returned data is an associative array with these
 *         fields:
 *           - id
 *           - username
 *           - password
 *           - auth_token
 */
function getUserInfoByAuthToken($auth_token){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "SELECT * FROM users WHERE auth_token = :auth_token");
        $success = $statement->execute(array(
            ":auth_token" => $auth_token));
           
        if(!$success){
            die("There was an error reading from the database: ". 
                $dbh->errorInfo()[2]);
        } else {
            return $statement->fetch(PDO::FETCH_ASSOC);
        }
    } catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
}

/**
 * Updates  data for the given auth token. 
 * 
 * @param userId The id of the user.
 * @param authToken The auth token to assign the user.
 */
function setUserAuthToken($userId, $authToken){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "UPDATE users SET auth_token = :auth_token ".
            "WHERE id = :user_id ");

        $success = $statement->execute(array(
            ":auth_token" => $authToken,
            ":user_id" => $userId));

        if(!$success){
            die("There was an error updating the database: ". 
                $dbh->errorInfo()[2]);
        } 
    } catch(PDOException $e){
        die("There was an error updating the database: ". $e->getMessage());
    }
}