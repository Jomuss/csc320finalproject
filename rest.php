<?php

// require_once("handler.php");
$prefix = "/~jmoss/csc302-project/rest.php";



$routes = [
    // // GET /books  should call listBooks
    // makeRoute("GET", "#^/books/?(\?.*)?$#", "listBooks"),
    // // POST /books should call addBook
    // makeRoute("POST", "#^/books/?(\?.*)?$#", "addBook"),
    // // PATCH /books/:id should call updateBook
    // makeRoute("PATCH", "#^/books/(\w+)/?(\?.*)?$#", "updateBook"),
    // // PUT /books/:id should call replaceBook
    // makeRoute("PUT", "#^/books/(\w+)/?(\?.*)?$#", "replaceBook"),
    // // DELETE /books/:id should call deleteBook
    // makeRoute("DELETE", "#^/books/(\w+)/?(\?.*)?$#", "deleteBook"),
    // // GET / should call showRoot
    // makeRoute("GET", "#^/$#", "showRoot")

    makeRoute("POST", "#^/upload-settings/?(\?.*)?$#", "uploadSettings"),

    makeRoute("POST", "#^/edges/?(\?.*)?$#", "addEdges"),
];

// Initial book keeping.
// Get the path and strip off the prefix (e.g., ~juser/dir1/dir2/script.php).
$uri = preg_replace("#^". $prefix ."/?#", "/", $_SERVER['REQUEST_URI']);
// Get the request method; PHP doesn't handle non-GET or POST requests
// well, so we'll mimic them with POST requests with a "_method" param
// set to the method we want to use.
$method = $_SERVER["REQUEST_METHOD"];
$params = $_GET;
if($method == "POST"){
    $params = $_POST;
    if(array_key_exists("_method", $_POST))
        $method = $_POST["_method"];
} 

$foundMatchingRoute = false;
$match = [];
foreach($routes as $route){
    if($method == $route["method"]){
        preg_match($route["pattern"], $uri, $match);
        if($match){
            die(json_encode($route["controller"]($uri, $match, $params)));
            $foundMatchingRoute = true;
        }
    }
}

if(!$foundMatchingRoute){
    echo "No route found for: $uri";
}



function makeRoute($method, $pattern, $function){
    return [
        "method" => $method,
        "pattern" => $pattern,
        "controller" => $function
    ];
}

function uploadSettings($uri, $matches, $params){
    global $loggedInUser;
    bootUserIfNotLoggedIn();
    addNewUserSettings($params["settingsText"], $params["settingsTitle"]);
    

}
