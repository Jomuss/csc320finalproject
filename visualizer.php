<!--  File:        visualizer.php
 Author:      Joe Moss
 Date:        212-Dec-2018
 Purpose:  This is the file that presents the newtork visuzlier and the UI to interact with it -->   
<!DOCTYPE >
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS  -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<link rel="stylesheet" href="custom.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<script src="https://d3js.org/d3.v3.min.js"></script>
	<!-- Contains all of the logic pertaining to the graphing and tables -->
	<script src="js/graphing.js"></script>
	<script src="js/saveSvgAsPng.js"></script>
	<script>
		

		var uploadSettings = function(event){

			$.ajax({
				url: 'handler.php',
				method: 'post',
				data: {
					action : "upload_settings",
					settingsTitle : $("#settings-name").val(),
					settingsText : JSON.stringify(settings)
				},
				success: (response)=>{
                    
                    var data = JSON.parse(response);
                    if(data.success){
                       $("#upload-settings-modal").modal("hide");
                       $("#success-alert").show();
                       $("#settings-table > tbody:last-child").append('<tr><td><input type="checkbox" name="'+$("#settings-name").val()+'">'+
                       		$("#settings-name").val()+'</td></tr>');		

                    } else {
                        alert(data.error);
                    }
                },
                error: (xhr, status, error)=>{
                    alert(error);
                    
                }			
            });
			
		};

		var getSettings = function(event){
			
			$.ajax({
				url: 'handler.php',
				method: 'post',
				data: {
					action : "download_settings",
					settings_title : $("input[id='settings-checkbox']:checked").parent().text()
				},
				success: function (data) {
                   
                    var stringData = JSON.stringify(data);
                    //Get JSON server success response from string
                    var trimmedData = data.replace("{\"success\":true}", " ");
                    data = JSON.parse(trimmedData);
                    if(trimmedData != stringData){
                    	data["success"] = true;
                    }else{
                    	data["success"] = false;
                    }	

                    if(data.success){
                    	updateSettingsFromSelected(data);
                    	$("#choose-settings-modal").modal("hide");
	

                    } else {
                        alert(data.error);
                    }
                },
                error: (xhr, status, error)=>{
                    alert(error);
                    
                }			
            });
   					
		};


		

		$(document).ready(function(){
			$(document).on('click', '#upload-settings-button', uploadSettings);
			$(document).on('click', '#download-settings-button', function () {
				$("#choose-settings-modal").modal('show');

			});
			$(document).on('click', '#apply-settings-button', getSettings);

			$("#success-alert").on("close.bs.alert", function () {
      				$("#success-alert").hide();
      				return false;
			});
		});
	</script>
	
</head>

<body>
	<nav class="navbar navbar-inverse">
		<div class="navbar-header">
      		<p style="margin-bottom: 0"class="navbar-brand">Network Visualization Tool</p>
      	</div>
      		<ul class="nav navbar-nav">

				<li><a href="#">Logout</a></li>
			</ul>
	</nav>
	<div id="success-alert"class="alert alert-success collapse" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
		You succesfully saved your settings!
	</div>
	<div id="main" class="container-fluid row">
		<div class="col-lg-7">
			<h1>Network Data</h1>
			<div id="data" class="col-lg-4 column">
				<div class="table-responsive">
					<h3>Node Table</h3>
						<table class="table table-bordered" id="nodeTable"></table>
						Color Column: <select id="colorSelector" class="nodeSelector">
							<option>N/A</option>
							 
						</select><br>
						Label Column: <select id="labelSelector" class="nodeSelector">
							<option>N/A</option>
						</select>
				</div>
			</div>
			<div id="data" class="col-lg-4 column">
				<div class="table-responsive">
					<h3>Edge Table</h3>
					<table class="table table-bordered" id="edgeTable"></table>
					Source Column: <select id="sourceSelector" class="edgeSelector">
						<option>N/A</option> 
					</select><br>
					Destination Column: <select id="destinationSelector" class="edgeSelector">
						<option>N/A</option>
					</select>
				</div>
			</div>
			<div id="data" class="col-lg-4 column">
				<h3>Node and Edge Data</h3>
				Select Node File: <input class="upload-button" id="nodeFileInput" type="file" name="nodeFileInput"/>
				Select Edge File: <input class="upload-button" type="file" name="edgeFileInput" id="edgeFileInput"/>
				<button class="btn btn-primary upload-button" id="graphInitButton" type="submit">Initialize/Refresh Graph</button>
				
				
				<p id="numNodes">Number of Nodes: </p>
				<p id="numEdges">Number of Edges: </p>
				<h4 id="selectedNodeData">Selected Node Data</h4></br>
				<div>
					
				</div>


			</div>
		</div>
		<div class="col-lg-5">
			<h1>Graph</h1>
			<div id="graph-container"></div>
			<div id="graph-settings">
				<h5 style="margin-left: .5em">Graph Settings</h5>
				<p style="margin-left: .5em">Show Node Names <input id="names-toggle" type="checkbox" checked></p>
				<p style="margin-left: .5em">Edge Color: <input id="edge-color" type="color"></p>
				<button id="refresh-button">Apply Changes</button> <button id="upload-settings-modal-button">Upload Current Settings</button>
				<button id="download-graph-button">Download Graph</button><button id="download-settings-button">Saved Settings</button>
				
				<div style="clear:both"></div>

				
			</div>
		</div>
	</div>
	<!-- Modal for downloading graph -->
	<div id="download-modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Please choose your download format</h4>
				</div>
				
					<button id="download-png-button">Save as .png</button>
					<button id="download-graphml-button" disabled="disabled">Save as .graphml (disabled)</button>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal for uploading settings -->
	<div id="upload-settings-modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Upload settings</h4>
					<br>
				</div>	
				<p style="margin-top: 1.5em; margin-bottom: 1.5em; margin-left: 1.5em">
						Settings Name: <input type="text" name="settings-name" id="settings-name">
						<button "type="submit" id="upload-settings-button">Upload Settings</button>
				</p>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal for choosing from uploaded settings -->
	<div id="choose-settings-modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Choose Settings</h4>
					<br>
				</div>	
				<table class="table" id="settings-table">
				<?php foreach(getUserSettingsNames($loggedInUser) as $settings_name) { ?>
					<tr>
						<td><input id="settings-checkbox" type="checkbox"><?= $settings_name["settings_title"]?></td>
<!-- 						
 -->					</tr>
					
					
				<?php } ?>
				</table>
				<div class="modal-footer">
					<button type="button" id="apply-settings-button" class="btn btn-default">Apply Chosen Settings</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
